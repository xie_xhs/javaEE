package com.xhs.controller;

import com.xhs.entity.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MyController {
    @GetMapping("/admin")
    public String admin(){
        return "admin";
    }
    @GetMapping("/main")
    public String main(){
        return "main";
    }
    @GetMapping("/manage")
    public String manage(){
        return "manage";
    }
    @GetMapping("/index")
    public String index(){
        return "index";
    }
    @GetMapping("/login")
    public String login(){
        return "login";
    }
    @GetMapping("/401")
    public String error_401(){
        return "401";
    }
    @PostMapping("/login")
    public String login_post(String username, String password, Model model){

        Subject subject  = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        try {
            subject.login(token);
            //校验完成,登录成功
            SysUser user = (SysUser) subject.getPrincipal();
            subject.getSession().setAttribute("username",user.getUsername());
            return "index";
        } catch (UnknownAccountException e) {
            //用户不存在异常
            //e.printStackTrace();
            model.addAttribute("msg","用户名错误！");
            return "login";
        } catch (IncorrectCredentialsException e){
            //密码错误异常
            //e.printStackTrace();
            model.addAttribute("msg","密码错误！ ");
            return "login";
        }


    }
    @GetMapping("/logout")
    public String logout(){
        Subject subject  = SecurityUtils.getSubject();
        subject.logout();
        return "index";
    }
}
