package com.xhs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xhs.entity.SysUser;
import com.xhs.mapper.UserMapper;
import com.xhs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public SysUser findByUsername(String username) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("username",username);
        return userMapper.selectOne(wrapper);
    }
}
