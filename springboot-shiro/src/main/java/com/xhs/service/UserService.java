package com.xhs.service;

import com.xhs.entity.SysUser;

public interface UserService {
    SysUser findByUsername(String username);
}
