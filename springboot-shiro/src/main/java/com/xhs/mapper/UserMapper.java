package com.xhs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xhs.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<SysUser> {

}
