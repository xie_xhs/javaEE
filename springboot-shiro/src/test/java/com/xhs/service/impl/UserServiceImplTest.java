package com.xhs.service.impl;

import com.xhs.entity.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class UserServiceImplTest {
    @Autowired
    private UserServiceImpl userService;
    @Test
    void findByUsername() {
        SysUser admin = userService.findByUsername("admin");
        System.out.println(admin);
    }
}