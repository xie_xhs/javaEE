package com.xhs.mapper;

import com.xhs.entity.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class UserMapperTest {
    @Autowired
    private UserMapper userMapper;
    @Test
    public void userMapperTest(){
        List<SysUser> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }
}