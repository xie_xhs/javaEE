package com.xhs.entity;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author xhs
 * @since 2021-07-22 19:15:26
 */
public class User implements Serializable {
    private static final long serialVersionUID = -22645307632881052L;
    
    private Integer id;
    
    private String name;
    
    private String pwd;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

}