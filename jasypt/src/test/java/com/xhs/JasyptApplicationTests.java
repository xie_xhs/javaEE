package com.xhs;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class JasyptApplicationTests {

    @Test
    void contextLoads() {

    }

    public static void main(String[] args) {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        standardPBEStringEncryptor.setAlgorithm("PBEWithMD5AndDES");
        standardPBEStringEncryptor.setPassword("xiehusheng08");
        String root = standardPBEStringEncryptor.encrypt("com.mysql.cj.jdbc.Driver");
        System.out.println("加密后："+root);
        String decrypt = standardPBEStringEncryptor.decrypt(root);
        System.out.println("解密后："+decrypt);
        String decrypt1 = standardPBEStringEncryptor.decrypt("EiALXqSp3XiPU2RNSMlrVV+Hc95CM4svhiMBstjHtKjiyaIDOPybZEdvGbWh5Z/JsrmHySYK7rSAFxG8DXVaJuaJAbj4EYE0B5nqQRo0Gh8X5/o2Ng4yg6niwFMFNV7EnvoUlH/BZNHB7oUCBKH4hqpAYlevHzSC");
        System.out.println("解密后："+decrypt1);
    }

}
