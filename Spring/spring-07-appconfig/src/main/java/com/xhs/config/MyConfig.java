package com.xhs.config;

import com.xhs.demo1.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.xhs.demo1")
//@Import()
public class MyConfig {
    @Bean
    public User getUser(){
        return new User();
    }
}
