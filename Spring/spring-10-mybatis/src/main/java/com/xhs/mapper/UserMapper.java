package com.xhs.mapper;

import com.xhs.pojo.User;

import java.util.List;

public interface UserMapper {
    List<User> getUserList();
}
