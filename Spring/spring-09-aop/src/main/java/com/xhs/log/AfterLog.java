package com.xhs.log;

import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

public class AfterLog implements AfterReturningAdvice {
    /**
     *
     * @param o
     * @param method
     * @param objects
     * @param target
     * @throws Throwable
     */
    public void afterReturning(Object o, Method method, Object[] objects, Object target) throws Throwable {
        System.out.println("执行了" + target.getClass().getName()
                +"的"+method.getName()+"方法,"
                +"返回值："+target);
    }
}
