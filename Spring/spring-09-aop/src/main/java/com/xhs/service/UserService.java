package com.xhs.service;

public interface UserService {
    void add();
    void delete();
    void update();
    void query();
}
