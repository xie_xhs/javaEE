package com.xhs.demo1;
//中介
public class Proxy implements Rent{
    private Host host;

    public Proxy() {
    }

    public Proxy(Host host) {
        this.host = host;
    }

    //租房
    public void rent() {
        seeHouse();
        host.rent();
        fee();
    }

    //看房子
    public void seeHouse(){
        System.out.println("带客户看房子");
    }
    //收中介费
    public void fee(){
        System.out.println("收中介费");
    }
}
