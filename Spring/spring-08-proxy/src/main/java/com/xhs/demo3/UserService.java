package com.xhs.demo3;

public interface UserService {

    void add();
    void delete();
    void update();
    void query();
}
