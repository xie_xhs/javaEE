package com.xhs.demo2;

public class Client {
    public static void main(String[] args) {
//        UserServiceImpl userService = new UserServiceImpl();
//        userService.add();
//        userService.update();
//        userService.delete();
//        userService.query();

        UserServiceImpl userService = new UserServiceImpl();
        UserServiceProxy userServiceProxy = new UserServiceProxy();
        userServiceProxy.setUserService(userService);
        userServiceProxy.add();
        userServiceProxy.update();
        userServiceProxy.delete();
        userServiceProxy.update();
    }
}
