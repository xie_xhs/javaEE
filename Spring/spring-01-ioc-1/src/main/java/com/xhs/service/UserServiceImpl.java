package com.xhs.service;

import com.xhs.mapper.UserDao;

public class UserServiceImpl implements UserService {
    private UserDao userDao;

    //利用set动态实现值得注入
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void getUser() {
        userDao.getUser();
    }
}
