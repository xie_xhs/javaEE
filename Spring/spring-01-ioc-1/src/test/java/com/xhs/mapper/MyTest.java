package com.xhs.mapper;

import com.xhs.service.UserServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    public void test(){
//        UserServiceImpl userService = new UserServiceImpl();
//        userService.setUserDao(new UserDaoOracleImpl());
//        userService.getUser();


        ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        UserServiceImpl oracleImpl = context.getBean("UserServiceImpl", UserServiceImpl.class);
        oracleImpl.getUser();
    }
}
