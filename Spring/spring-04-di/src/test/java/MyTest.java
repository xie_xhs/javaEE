import com.xhs.demo1.Student;
import com.xhs.demo1.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    public void test(){
        ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        Student student = context.getBean("student", Student.class);
        System.out.println(student.toString());
        /**
         * Student{
         * name='张三',
         * address=Address{address='北部湾大学'},
         * books=[红楼梦, 三国演义, 水浒传, 西游记],
         * hobbys=[听歌, 敲代码, 打游戏],
         * card={身份证=12345678904868546, 银行卡=12345678904868546},
         * games=[LOL, DNF, CSGO],
         * wife='null',
         * info={学号=12346, 性别=男性, 姓名=小米}}
         */
    }
    @Test
    public void test2() {
        ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        User user = context.getBean("user2", User.class);
        System.out.println(user.toString());

    }
}
