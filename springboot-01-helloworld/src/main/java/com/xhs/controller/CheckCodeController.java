package com.xhs.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.lang.Console;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckCodeController {
    //测试使用hutool工具生成验证码
    @GetMapping(value = "/api/user/checkCode.jpg")
    public void code() {
        //定义图形验证码的长和宽
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200, 100);
        //图形验证码写出，可以写出到文件，也可以写出到流
//        lineCaptcha.write("d:/checkCode.jpg");
        String codeBase64 = lineCaptcha.getImageBase64();
        //输出code
        Console.log(lineCaptcha.getCode());
        Console.log("====================================");
        Console.log(codeBase64);
        //验证图形验证码的有效性，返回boolean值
        boolean verify = lineCaptcha.verify("1234");
        Console.log(verify);
        //重新生成验证码
//        lineCaptcha.createCode();
//        lineCaptcha.write("d:/line.jpg");
        //新的验证码
//        Console.log(lineCaptcha.getCode());
        //验证图形验证码的有效性，返回boolean值
//        lineCaptcha.verify("1234");
    }
}
