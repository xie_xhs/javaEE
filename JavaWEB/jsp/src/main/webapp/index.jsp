<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: xhs
  Date: 2020/10/29
  Time: 10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--jsp表达式--%>
<%= new Date() %>
<%--jsp脚本片段--%>
<%
    int sum = 0;
    for (int i = 0; i < 100; i++) {
        sum += 1;

    }
%>
<%!
    static {
        System.out.println("全局代码");
    }
%>
<h1>index.jsp</h1>
</body>
</html>
