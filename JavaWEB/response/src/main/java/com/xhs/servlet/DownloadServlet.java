package com.xhs.servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;

@WebServlet( "/downloadServlet")
public class DownloadServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取文件的路径
        String realPath = getServletContext().getRealPath("/index.html");
        System.out.println("下载文件的路径："+realPath);
        //得到下载文件的文件名
        String fileName = realPath.substring(realPath.lastIndexOf("\\")+1);
        System.out.println("下载文件的文件名："+fileName);
        //设置浏览器支持()下载我们需要的文件
        response.setHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(fileName,"UTF-8"));
        //获取下载文件输入流
        FileInputStream is = new FileInputStream(realPath);
        //创建缓冲区
        int len = 0;
        byte[] buffer = new byte[1024];
        //获取OutputStream对象
        ServletOutputStream os = response.getOutputStream();
        //将FileInputStream流写入buffer缓冲区
        while ((len=is.read(buffer))>0){
            os.write(buffer,0,len);
        }
        //关闭流，释放资源
        is.close();
        os.close();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
