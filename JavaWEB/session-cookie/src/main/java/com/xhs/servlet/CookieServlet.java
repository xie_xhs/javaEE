package com.xhs.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@WebServlet("/c1")
public class CookieServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        //获取客户端的cookies
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            writer.write("你上一次访问的时间是：");
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("loginTime")){
                    //获取cookie的值
                    long l = Long.parseLong(cookie.getValue());
                    Date date = new Date(l);
                    writer.write(date.toLocaleString());
                }
            }
        } else {
            writer.write("这是你的第一次访问本站");
        }
        //响应cookie
        Cookie cookie = new Cookie("loginTime", System.currentTimeMillis()+"");
        //设置cookie到期时间
        cookie.setMaxAge(24*60*60);
        resp.addCookie(cookie);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
