package com.xhs.dao;

import com.xhs.pojo.Users;
import com.xhs.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class UsersDaoTest {
    @Test
    public void test1(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UsersMapper mapper = sqlSession.getMapper(UsersMapper.class);
        List<Users> usersList = mapper.getUsersList();
        for (Users users : usersList) {
            System.out.println(users);
        }
        sqlSession.close();
    }
    @Test
    public void test2(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UsersMapper mapper = sqlSession.getMapper(UsersMapper.class);
        int i = mapper.addUsers(new Users(5,"卢博","123456"));
        sqlSession.commit();//增删改，必须提交事务
        System.out.println((i>0)?"插入成功":"插入失败");
        sqlSession.close();
    }
    @Test
    public void test3(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UsersMapper mapper = sqlSession.getMapper(UsersMapper.class);
        int i = mapper.deleteUsersById(5);
        sqlSession.commit();//增删改，必须提交事务
        System.out.println((i>0)?"删除成功":"删除失败");
        sqlSession.close();
    }
}
