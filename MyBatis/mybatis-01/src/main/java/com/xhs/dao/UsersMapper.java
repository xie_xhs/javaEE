package com.xhs.dao;

import com.xhs.pojo.Users;

import java.util.List;

public interface UsersMapper {
    //查询全部数据
    List<Users> getUsersList();
    //添加一条数据
    int addUsers(Users user);
    //根据id删除数据
    int deleteUsersById(int id);
    //。。。CRUD
}
