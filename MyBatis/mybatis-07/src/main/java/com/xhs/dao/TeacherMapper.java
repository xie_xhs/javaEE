package com.xhs.dao;

import com.xhs.pojo.Teacher;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface TeacherMapper {
    //获取老师
    //List<Teacher> getTeacherList();
    //获取老师下所有的学生
    Teacher getTeacherList(@Param("tid") int id);
}
