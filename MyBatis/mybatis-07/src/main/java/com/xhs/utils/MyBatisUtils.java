package com.xhs.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * mybatis 加载工具类
 */
public class MyBatisUtils {
    private static SqlSessionFactory sqlSessionFactory;

    static {

        try {
            String resource = "mybatic-config.xml";
            InputStream inputstream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputstream);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 返回SqlSession对象
     * @return
     */
    public static SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }
}
