package com.xhs.dao;

import com.xhs.pojo.Teacher;
import com.xhs.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

public class TeacherMapperTest {
    @Test
    public void test1(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
        Teacher teacherList = mapper.getTeacherList(1);
        System.out.println(teacherList);
        sqlSession.close();
    }
}
