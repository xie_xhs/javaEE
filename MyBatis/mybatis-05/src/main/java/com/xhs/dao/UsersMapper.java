package com.xhs.dao;

import com.xhs.pojo.Users;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UsersMapper {
    //查询全部数据
    @Select("select * from user")
    List<Users> getUsersList();

    //根据id查数据
    @Select("select * from user where id = #{id} ")
    Users getUsersById(@Param("id") int id);

}
