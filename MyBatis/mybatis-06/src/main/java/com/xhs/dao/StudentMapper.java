package com.xhs.dao;

import com.xhs.pojo.Student;

import java.util.List;

public interface StudentMapper {
    List<Student> getStudentList();
    List<Student> getStudentList2();
}
