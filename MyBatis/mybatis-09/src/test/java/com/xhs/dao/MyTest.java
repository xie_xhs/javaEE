package com.xhs.dao;

import com.xhs.pojo.User;
import com.xhs.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

public class MyTest {
    @Test
    public void test1(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user1 = mapper.queryUserById(1);
        System.out.println(user1);
        //刷新一级缓存
        //sqlSession.clearCache();

        System.out.println("=========================================");
        User user2 = mapper.queryUserById(1);
        System.out.println(user2);

        System.out.println(user1==user2);
        sqlSession.close();
    }
}
