package com.xhs.dao;

import com.xhs.pojo.Users;
import com.xhs.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

public class UsersDaoTest {
    @Test
    public void test1() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UsersMapper mapper = sqlSession.getMapper(UsersMapper.class);
        List<Users> usersList = mapper.getUsersList();
        for (Users users : usersList) {
            System.out.println(users);
        }
        sqlSession.close();
    }

    @Test
    public void test2() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UsersMapper mapper = sqlSession.getMapper(UsersMapper.class);
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("starIndex", 0);
        map.put("pageSize", 2);
        List<Users> usersLimit = mapper.getUsersLimit(map);
        for (Users users : usersLimit) {
            System.out.println(users);
        }

        sqlSession.close();
    }
}
