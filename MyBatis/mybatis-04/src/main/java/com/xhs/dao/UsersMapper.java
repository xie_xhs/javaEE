package com.xhs.dao;

import com.xhs.pojo.Users;

import java.util.List;
import java.util.Map;

public interface UsersMapper {
    //查询全部数据
    List<Users> getUsersList();
    //分页
    List<Users> getUsersLimit(Map<String,Integer> map);
}
