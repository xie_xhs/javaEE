package com.xhs.dao;

import com.xhs.pojo.Users;

import java.util.List;

public interface UsersMapper {
    //查询全部数据
    List<Users> getUsersList();
}
