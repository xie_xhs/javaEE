package com.xhs.dao;

import com.xhs.pojo.Users;
import com.xhs.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class UsersDaoTest {
    @Test
    public void test1(){
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        UsersMapper mapper = sqlSession.getMapper(UsersMapper.class);
        List<Users> usersList = mapper.getUsersList();
        for (Users users : usersList) {
            System.out.println(users);
        }
        sqlSession.close();
    }

}
