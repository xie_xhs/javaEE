package com.xhs.dao;

import com.xhs.pojo.Blog;
import com.xhs.utils.IDutils;
import com.xhs.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class TeacherMapperTest {
    @Test
    public void test1() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        Blog blog = new Blog();
        blog.setId(IDutils.getID());
        blog.setTitle("Mybatis");
        blog.setAuthor("狂神说");
        blog.setCreateTime(new Date());
        blog.setViews(9999);

        mapper.addBook(blog);

        blog.setId(IDutils.getID());
        blog.setTitle("Java");
        mapper.addBook(blog);

        blog.setId(IDutils.getID());
        blog.setTitle("Spring");
        mapper.addBook(blog);

        blog.setId(IDutils.getID());
        blog.setTitle("微服务");
        mapper.addBook(blog);

        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void test2() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        HashMap map = new HashMap();
//        map.put("title","微服务");
//        map.put("author","ssm");
        List<Blog> blogs = mapper.queryBlogIf(map);
        for (Blog blog : blogs) {
            System.out.println(blog);
        }

        sqlSession.close();
    }

    @Test
    public void test3() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        HashMap map = new HashMap();
//        map.put("title", "微服务");
        map.put("author", "ssm");
//        map.put("views", "9999");
        List<Blog> blogs = mapper.queryBlogChoose(map);
        for (Blog blog : blogs) {
            System.out.println(blog);
        }
        sqlSession.close();
    }

    @Test
    public void test4() {
        SqlSession sqlSession = MyBatisUtils.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        HashMap map = new HashMap();
        map.put("title", "微服务");
        map.put("author", "ssm");
        map.put("id", "fc18cd70115c4ea1a677dc3727795867");
        int i = mapper.updateBlog(map);
        System.out.println(i);
        sqlSession.commit();
        sqlSession.close();
    }
}
