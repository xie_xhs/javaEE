package com.xhs.dao;

import com.xhs.pojo.Blog;

import java.util.List;
import java.util.Map;

public interface BlogMapper {
    //插入数据
    int addBook(Blog blog);
    //查询Blog
    List<Blog> queryBlogIf(Map map);
    //根据条件查询Blog
    List<Blog> queryBlogChoose(Map map);
    //更新Blog
    int updateBlog(Map map);
}
