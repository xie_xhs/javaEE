package com.xhs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller //说明这个类是一个controller
@ResponseBody //说明这个类下面的方法返回都是字符串
//@RestController //使用这个可以等效前面两个注解,因为这个注解上已经加了这个@Controller和@ResponseBody

public class HelloController {

    @RequestMapping(value = "/api/hello", method = RequestMethod.GET)
//    @GetMapping(value = "/api/hello") //也可以使用这个注解，表明这个GET请求
    public String hello() {
        return "hello SpringBoot !";
    }
}
