package com.xhs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ControllerTest3 {
    @RequestMapping("")
    public String test(Model model){
        model.addAttribute("msg","大洼大洼");
        return "";
    }
}
