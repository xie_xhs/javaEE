package com.xhs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ControllerTest2 {
    @RequestMapping("/test2")
    public String test(Model model){
        model.addAttribute("msg","dhwayda");
        return "test2";
    }
}
