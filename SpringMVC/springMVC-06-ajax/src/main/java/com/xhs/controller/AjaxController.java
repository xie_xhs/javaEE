package com.xhs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class AjaxController {
    @RequestMapping("/a1")
    @ResponseBody
    public String ajax(){
        return "hello";
    }

    @ResponseBody
    @PostMapping("/a2")
    public void ajax2(String name, HttpServletResponse response) throws IOException {
        System.out.println("a2:param=>"+name);
        if ("xhs".equals(name)){
            response.getWriter().println("true");
        }else {
            response.getWriter().println("false");
        }
    }
}
