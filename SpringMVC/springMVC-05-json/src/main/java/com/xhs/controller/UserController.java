package com.xhs.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xhs.pojo.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
public class UserController {
    @ResponseBody
    @RequestMapping("/json")
    public String json1() throws JsonProcessingException {
        List<User> list = new ArrayList<User>();
        User user1 = new User(1, "苏打水", 222);
        User user2 = new User(1, "苏打水", 222);
        User user3 = new User(1, "苏打水", 222);
        User user4 = new User(1, "苏打水", 222);
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        return new ObjectMapper().writeValueAsString(list);
    }
}
