package com.xhs.mapper;

import com.xhs.entity.Books;
import junit.framework.TestCase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class BooksMapperTest extends TestCase {
    @Autowired
    private BooksMapper booksMapper;
    @Test
    public void test(){
        List<Books> books = booksMapper.queryAllBook();
        for (Books book : books) {
            System.out.println(book);
        }
    }
}