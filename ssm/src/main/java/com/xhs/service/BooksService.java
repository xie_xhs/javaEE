package com.xhs.service;

import com.xhs.entity.Books;

import java.util.List;

public interface BooksService {
    List<Books> queryAllBook();
}
