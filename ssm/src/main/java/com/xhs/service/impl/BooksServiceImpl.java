package com.xhs.service.impl;

import com.xhs.entity.Books;
import com.xhs.mapper.BooksMapper;
import com.xhs.service.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BooksServiceImpl implements BooksService {
    @Autowired
    private BooksMapper booksMapper;
    @Override
    public List<Books> queryAllBook() {
        return booksMapper.queryAllBook();
    }
}
