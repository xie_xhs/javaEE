<%--
  Created by IntelliJ IDEA.
  User: xhs
  Date: 2020/10/31
  Time: 21:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <c:forEach items="${list}" var="book">
        ${book.bookID}--${book.bookName}--${book.bookCounts}--${book.detail}
    </c:forEach>
</body>
</html>
